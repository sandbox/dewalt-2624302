<?php

/**
 * @file
 * Module file for Entity Revision.
 */

/**
 * Entity revision action status - update current, make it actual.
 */
define('ENTITY_REVISION_UPDATE_AND_USE', 0);

/**
 * Entity revision action status - create new revision, make it actual.
 */
define('ENTITY_REVISION_CREATE_AND_USE', 1);

/**
 * Entity revision action status - update current revision.
 */
define('ENTITY_REVISION_UPDATE_NO_USE', 2);

/**
 * Entity revision action status - create new revision.
 */
define('ENTITY_REVISION_CREATE_NO_USE', 3);

/**
 * Include field SQL storage implementation.
 */
module_load_include('inc', 'entity_revision', 'storage/field_sql_storage');

/**
 * Include node module additions.
 */
module_load_include('inc', 'entity_revision', 'modules/node');

/**
 * Include taxonomy_revision module additions.
 */
module_load_include('inc', 'entity_revision', 'modules/taxonomy_revision');

/**
 * Include field_collection module additions.
 */
module_load_include('inc', 'entity_revision', 'modules/field_collection');

/**
 * Implements hook_module_implement_alter().
 */
function entity_revision_module_implements_alter(&$implementations, $hook) {
  // Set order of replaced hook emplementation execution.
  if (isset($implementations['entity_revision'])) {
    switch ($hook) {
      // The following hook implementations should be executed as last ones.
      case 'entity_info_alter':
      case 'menu_alter':
      case 'form_alter':
        $group = $implementations['entity_revision'];
        unset($implementations['entity_revision']);
        $implementations['entity_revision'] = $group;
        break;

      // The following hook implementations should be executed as first ones.
      case 'entity_insert':
      case 'entity_update':
      case 'entity_revision_insert':
      case 'entity_revision_update':
        $group = $implementations['entity_revision'];
        unset($implementations['entity_revision']);
        $implementations = array('entity_revision' => $group) + $implementations;
        break;
    }
  }

  if (
    // The following hooks aren't correct in taxonomy_revision-7.x-1.2.
    $hook == 'taxonomy_term_insert' ||
    $hook == 'taxonomy_term_update' ||
    $hook == 'taxonomy_term_presave' ||
    // The following hooks aren't compatible in taxonomy_revision-7.x-1.3.
    $hook == 'field_storage_pre_insert' ||
    $hook == 'field_storage_pre_update'
  ) {
    unset($implementations['taxonomy_revision']);
  }
}

/**
 * Implements hook_hook_info().
 */
function entity_revision_hook_info() {
  $hooks = array(
    'entity_revision_supported_entities',
    'entity_revision_entity_info_alter',
    'entity_revision_permission',
    'entity_revision_menu_alter',
    'entity_revision_form_callbacks_replacements',
    'entity_revision_access',
    'entity_revision_access_alter',
    'entity_revision_options_alter',
  );
  return array_fill_keys($hooks, array('group' => 'entity_revision'));
}

/**
 * Implements hook_entity_info_alter().
 */
function entity_revision_entity_info_alter(&$entity_info) {
  $supported_entities = entity_revision_get_supported_entities();

  foreach ($supported_entities as $entity_type => $module) {
    if (isset($entity_info[$entity_type]) && !empty($entity_info[$entity_type]['revision table'])) {
      $entity_info[$entity_type]['revision'] = array(
        'module' => $module,
      );
      // Own hook implemented to allow modify entity info by supporting module
      // only. Like to drupal_alter('entity_revision_entity_info', $info) for
      // target module.
      if (module_hook($module, __FUNCTION__)) {
        $function = $module . '_' . __FUNCTION__;
        $function($entity_info[$entity_type], $entity_type);
      }
    }
  }
}

/**
 * Implements hook_permission().
 */
function entity_revision_permission() {
  $supported_entities = entity_revision_get_supported_entities();

  $permissions = array();
  foreach ($supported_entities as $entity_type => $module) {
    // Own hook implemented to allow set permissions by supporting module
    // only.
    if ($module_permissions = module_invoke($module, __FUNCTION__, $entity_type)) {
      $permissions += $module_permissions;
    }
  }

  return $permissions;
}

/**
 * Implements hook_menu_alter().
 */
function entity_revision_menu_alter(&$items) {
  $supported_entities = entity_revision_get_supported_entities();

  foreach ($supported_entities as $entity_type => $module) {
    // Own hook implemented to not affect menu tree, it the module is disabled.
    // Like to drupal_alter('entity_revision_menu', $items) for target module.
    if (module_hook($module, __FUNCTION__)) {
      $function = $module . '_' . __FUNCTION__;
      $function($items, $entity_type);
    }
  }
}

/**
 * Implements hook_entity_load().
 */
function entity_revision_entity_load($entities, $type) {
  $info = entity_get_info($type);

  if (!isset($info['revision']['module']) || empty($entities)) {
    foreach ($entities as $entity) {
      // Key to provide default behaviour on fields update.
      // @see entity_revision_field_storage_write().
      $entity->current_revision = TRUE;
    }
    return;
  }

  $key = $info['entity keys']['id'];
  $revision_key = $info['entity keys']['revision'];
  $keys = db_select($info['base table'], 't')
    ->fields('t', array($key, $revision_key))
    ->condition($key, array_keys($entities))
    ->execute()
    ->fetchAllKeyed();

  foreach ($entities as $entity) {
    list($id, $revision_id,) = entity_extract_ids($type, $entity);
    $entity->current_revision_id = isset($keys[$id]) ? $keys[$id] : NULL;
    $entity->current_revision = $entity->current_revision_id == $revision_id;
  }
}

/**
 * Implements hook_entity_insert().
 */
function entity_revision_entity_insert($entity, $type) {
  entity_revision_entity_load(array($entity), $type);
}

/**
 * Implements hook_entity_update().
 */
function entity_revision_entity_update($entity, $type) {
  entity_revision_entity_load(array($entity), $type);
}

/**
 * Implements hook_entity_revision_insert().
 */
function entity_revision_entity_revision_insert($entity, $type) {
  entity_revision_entity_load(array($entity), $type);
}

/**
 * Implements hook_entity_revision_update().
 */
function entity_revision_entity_revision_update($entity, $type) {
  entity_revision_entity_load(array($entity), $type);
}

/**
 * Implements hook_form_alter().
 */
function entity_revision_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form_state['build_info']['base_form_id'])) {
    $form_id = $form_state['build_info']['base_form_id'];
  }

  $replacements = entity_revision_get_form_callbacks_replacements($form_id);
  if (!empty($replacements) && isset($replacements['entity_type'])) {
    // Check for possible array_merge_recursive() behaviour.
    if (is_array($replacements['entity_type'])) {
      $replacements['entity_type'] = end($replacements['entity_type']);
    }
    $entity_type = $replacements['entity_type'];
    $form_state['enitity_revision']['replacements'] = $replacements;
    $form['#process'][] = '_entity_revision_replace_callbacks';

    // Extract entity revision info.
    if (isset($replacements['entity_callback']) && function_exists($replacements['entity_callback'])) {
      $readonly_form = $form;
      $entity = $replacements['entity_callback']($readonly_form, $form_state);
    }
    elseif (isset($replacements['entity_key'])) {
      $parents = explode('][', trim($replacements['entity_key'], ']['));
      $entity = drupal_array_get_nested_value($form_state, $parents);
    }
    if (!empty($entity)) {
      $is_current = empty($entity->current_revision_id) || $entity->current_revision;
      list($entity_id, ,) = entity_extract_ids($entity_type, $entity);
    }
    else {
      $is_current = TRUE;
      $entity_id = NULL;
      $entity = NULL;
    }

    // Add revisions section to the form.
    $options = array(
      ENTITY_REVISION_UPDATE_AND_USE => $is_current ? t('Save content') : t('Save revision and use it'),
      ENTITY_REVISION_CREATE_AND_USE => t('Create new revision and use it'),
      ENTITY_REVISION_UPDATE_NO_USE => t('Save revision'),
      ENTITY_REVISION_CREATE_NO_USE => t('Create new revision'),
    );
    if (!$is_current && !entity_revision_access('use', $entity_type, $entity)) {
      unset($options[ENTITY_REVISION_UPDATE_AND_USE]);
      unset($options[ENTITY_REVISION_CREATE_AND_USE]);
    }
    if (empty($entity_id) || !entity_revision_access('create', $entity_type, $entity)) {
      unset($options[ENTITY_REVISION_CREATE_AND_USE]);
      unset($options[ENTITY_REVISION_CREATE_NO_USE]);
    }
    if ($is_current) {
      unset($options[ENTITY_REVISION_UPDATE_NO_USE]);
    }
    // Allow other modules limit or extend options.
    drupal_alter('entity_revision_options', $options, $entity_type, $entity);
    $revision_informaition = array(
      '#type' => 'fieldset',
      '#title' => t('Revision information'),
      '#collapsible' => TRUE,
      '#weight' => 20,
    );
    $default = isset($entity->revision, $options[(int) $entity->revision]) ? (int) $entity->revision : key($options);
    $revision_informaition['revision'] = array(
      '#type' => 'radios',
      '#title' => t('Create new revision'),
      '#options' => $options,
      '#default_value' => $default,
      '#access' => count($options) > 1,
    );

    if (empty($form['revision_information'])) {
      $form['revision_information'] = $revision_informaition;
    }
    else {
      $form['revision_information'] = $revision_informaition + $form['revision_information'];
    }
  }
}

/**
 * Get array of supported entities in key, with module supports it in value.
 *
 * @return array
 *   Array of supported entities in key, with module supports it in value.
 */
function entity_revision_get_supported_entities() {
  // Use the advanced drupal_static() pattern, since this is called very often.
  static $drupal_static_fast;
  if (!isset($drupal_static_fast)) {
    $drupal_static_fast['entity_revision_supported_entities'] = &drupal_static(__FUNCTION__);
  }
  $supported_entities = &$drupal_static_fast['entity_revision_supported_entities'];

  if (!isset($supported_entities)) {
    if ($cache = cache_get('entity_revision:supported_entities')) {
      $supported_entities = $cache->data;
    }
    else {
      $supported_entities = array();
      $result = module_invoke_all('entity_revision_supported_entities');
      // Normalize array_merge_recursive behavior.
      foreach ($result as $entity_name => $modules) {
        $module = is_array($modules) ? end($modules) : $modules;
        // Allow modules to disable support too with empty module name.
        if ($module) {
          $supported_entities[$entity_name] = $module;
        }
      }
      cache_set('entity_revision:supported_entities', $supported_entities);
    }
  }

  return $supported_entities;
}

/**
 * Get form callbacks replacement.
 *
 * @param string $form_id
 *   (optional) Base form ID.
 *
 * @return array
 *   Array of 'submit' and 'validate' callbacks for replacement for form, if
 *   param is specified, otherwise array of replacements for all forms, keyed
 *   by base form ID.
 */
function entity_revision_get_form_callbacks_replacements($form_id = NULL) {
  // Use the advanced drupal_static() pattern, since this is called very often.
  static $drupal_static_fast;
  if (!isset($drupal_static_fast)) {
    $drupal_static_fast['entity_revision_replacements'] = &drupal_static('entity_revision_replacements');
  }
  $replacements = &$drupal_static_fast['entity_revision_replacements'];

  if (!isset($replacements)) {
    if ($cache = cache_get('entity_revision:replacements')) {
      $replacements = $cache->data;
    }
    else {
      $replacements = module_invoke_all('entity_revision_form_callbacks_replacements');
      cache_set('entity_revision:replacements', $replacements);
    }
  }

  if ($form_id) {
    return isset($replacements[$form_id]) ? $replacements[$form_id] : NULL;
  }

  return $replacements;
}

/**
 * Get module, supporting content type revisions.
 *
 * @return mixed
 *   String with module name, or FALSE if not found any.
 */
function entity_revision_get_supporting_module($entity_type) {
  $info = entity_get_info($entity_type);
  if ($info && isset($info['revision']['module'])) {
    return $info['revision']['module'];
  }

  return FALSE;
}

/**
 * Title callback for revision default tab.
 */
function entity_revision_title($entity_type, $entity) {
  list(, $revision_id,) = entity_extract_ids($entity_type, $entity);
  return '#' . $revision_id;
}

/**
 * Check user access for operations with revisions.
 */
function entity_revision_access($op, $entity_type, $entity, $account = NULL) {
  // Forbid operations with actual content throught revision interfaces.
  if ($entity->current_revision && ($op == 'update' || $op == 'delete' || $op == 'use')) {
    return FALSE;
  }

  // Get entity supporting module.
  $module = entity_revision_get_supporting_module($entity_type);

  // Check enity-level permissions for operation, if provided.
  $function = $module . '_entity_revision_access';
  if (module_hook($module, 'entity_revision_access')) {
    switch ($op) {
      case 'create':
      case 'view':
      case 'use':
      case 'update':
      case 'delete':
        // Revisions operations aren't allowed for new entity.
        list($id, ,) = entity_extract_ids($entity_type, $entity);
        $access = $id && $function($op, $entity_type, $entity, $account);
        break;

      default:
        $access = FALSE;
    }
  }

  // Allow the modules to modify access.
  drupal_alter('entity_revision_access', $access, $op, $entity_type, $entity, $account);

  return $access;
}

/**
 * Element process function.
 *
 * Replaces element submit and validate callback, if needed.
 */
function _entity_revision_replace_callbacks(&$element, &$form_state, $complete_form) {
  // Replace validation callbacks.
  if (!empty($element['#validate'])) {
    foreach ($element['#validate'] as $position => $callback) {
      if (isset($form_state['enitity_revision']['replacements']['validate'][$callback])) {
        $element['#validate'][$position] = $form_state['enitity_revision']['replacements']['validate'][$callback];
      }
    }
  }
  // Replace submit callbacks.
  if (!empty($element['#submit'])) {
    foreach ($element['#submit'] as $position => $callback) {
      if (isset($form_state['enitity_revision']['replacements']['submit'][$callback])) {
        $element['#submit'][$position] = $form_state['enitity_revision']['replacements']['submit'][$callback];
      }
    }
  }
  // Replace entity builders.
  if (!empty($element['#entity_builders'])) {
    foreach ($element['#entity_builders'] as $position => $callback) {
      if (isset($form_state['enitity_revision']['replacements']['entity_builder'][$callback])) {
        $element['#entity_builders'][$position] = $form_state['enitity_revision']['replacements']['entity_builder'][$callback];
      }
    }
  }
  // Process children.
  foreach (element_children($element) as $key) {
    _entity_revision_replace_callbacks($element[$key], $form_state, $complete_form);
  }

  return $element;
}

/**
 * Helper function to update entity field collection fields reference.
 *
 * @param string $entity_type
 *   The type of entity on which the operation is to be performed.
 * @param object $entity
 *   The entity object on which the operation is to be performed.
 */
function _entity_revision_update_field_collection_fields($entity_type, $entity) {
  list(, , $bundle) = entity_extract_ids($entity_type, $entity);

  foreach (field_info_instances($entity_type, $bundle) as $instance) {
    if ($instance['widget']['type'] == 'field_collection_embed') {
      foreach ($entity->{$instance['field_name']} as $langcode => $items) {
        foreach ($items as $delta => $item) {
          $field_collection_item = field_collection_field_get_entity($item);
          if (empty($field_collection_item->is_new)) {
            // Can't use FieldCollectionItemEntity::updateHostEntity()
            // because of bug in it.
            $field_collection_item->is_new = TRUE;
            $field_collection_item->setHostEntity($entity_type, $entity, LANGUAGE_NONE, FALSE);
            unset($field_collection_item->is_new);
            $entity->{$instance['field_name']}[$langcode][$delta]['entity'] = $field_collection_item;
          }
        }
      }
    }
  }
}
