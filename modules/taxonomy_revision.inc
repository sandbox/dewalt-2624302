<?php

/**
 * @file
 * Provides entity revision hooks for taxonomy_revision module support.
 */

/**
 * Implements hook_taxonomy_term_presave().
 */
function entity_revision_taxonomy_term_presave($term) {
  if (!module_exists('taxonomy_revision')) {
    return;
  }

  global $user;
  $term_revision = clone($term);
  $term_revision->uid = $user->uid;
  $term_revision->timestamp = REQUEST_TIME;

  if (isset($term->revision) && in_array($term->revision, array(ENTITY_REVISION_CREATE_AND_USE, ENTITY_REVISION_CREATE_NO_USE))) {
    unset($term_revision->revision_id);
  }

  if (empty($term_revision->revision_id)) {
    drupal_write_record('taxonomy_term_data_revision', $term_revision);
  }
  else {
    drupal_write_record('taxonomy_term_data_revision', $term_revision, 'revision_id');
  }

  $term->revision_id = $term_revision->revision_id;
}

/**
 * Implements hook_taxonomy_term_insert().
 */
function entity_revision_taxonomy_term_insert($term) {
  if (!module_exists('taxonomy_revision')) {
    return;
  }

  db_update('taxonomy_term_data_revision')
    ->fields(array('tid' => $term->tid))
    ->condition('revision_id', $term->revision_id)
    ->execute();
}

/**
 * Implements hook_entity_revision_supported_entities().
 */
function taxonomy_revision_entity_revision_supported_entities() {
  return array('taxonomy_term' => 'taxonomy_revision');
}

/**
 * Implements hook_entity_revision_permission().
 */
function taxonomy_revision_entity_revision_permission() {
  return array(
    'update taxonomy term revisions' => array(
      'title' => t('Update content revisions'),
    ),
  );
}

/**
 * Implements hook_entity_revision_menu_alter().
 */
function taxonomy_revision_entity_revision_menu_alter(&$items, $entity_type) {
  if ($entity_type != 'taxonomy_term') {
    return;
  }

  $items['taxonomy/term/%taxonomy_term/revisions'] = array(
    'page callback' => 'taxonomy_revision_override_overview',
    'access callback' => 'entity_revision_access',
    'access arguments' => array('view', 'taxonomy_term', 2),
  ) + $items['taxonomy/term/%taxonomy_term/revisions'];

  $items['taxonomy/term/%taxonomy_revision_term/revisions/%'] = array(
    'title callback' => 'entity_revision_title',
    'title arguments' => array('taxonomy_term', 2),
    'access callback' => 'entity_revision_access',
    'access arguments' => array('view', 'taxonomy_term', 2),
  ) + $items['taxonomy/term/%taxonomy_revision_term/revisions/%/view'];

  $items['taxonomy/term/%taxonomy_revision_term/revisions/%/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['taxonomy/term/%taxonomy_revision_term/revisions/%/edit'] = array(
    'title' => 'Edit',
    'load arguments' => array(4),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('taxonomy_form_term', 2, NULL),
    'access callback' => 'entity_revision_access',
    'access arguments' => array('update', 'taxonomy_term', 2),
    'weight' => 0,
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    'file' => 'taxonomy.admin.inc',
    'file path' => drupal_get_path('module', 'taxonomy'),
  );

  $items['taxonomy/term/%taxonomy_revision_term/revisions/%/use'] = array(
    'title' => 'Make actual',
    'load arguments' => array(4),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('taxonomy_revision_use_confirm', 2),
    'access callback' => 'entity_revision_access',
    'access arguments' => array('use', 'taxonomy_term', 2),
    'weight' => 1,
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
  );

  $items['taxonomy/term/%taxonomy_revision_term/revisions/%/delete'] = array(
    'access callback' => 'entity_revision_access',
    'access arguments' => array('delete', 'taxonomy_term', 2),
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
  ) + $items['taxonomy/term/%taxonomy_revision_term/revisions/%/delete'];
}

/**
 * Implements hook_entity_revision_form_callbacks_replacements().
 */
function taxonomy_revision_entity_revision_form_callbacks_replacements() {
  return array(
    'taxonomy_form_term' => array(
      'entity_type' => 'taxonomy_term',
      'entity_key' => 'term',
      'submit' => array(
        'taxonomy_form_term_submit' => 'taxonomy_revision_form_term_submit',
      ),
      'entity_builder' => array(
        'taxonomy_revision_taxonomy_form_term_submit_build_term' => 'taxonomy_revision_build_term',
      ),
    ),
  );
}

/**
 * Implements hook_entity_revision_access().
 *
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
function taxonomy_revision_entity_revision_access($op, $entity_type, $entity, $account) {
  if (user_access('administer taxonomy', $account)) {
    return TRUE;
  }

  // If no user object is supplied, the access check is for the current user.
  if (empty($account)) {
    $account = $GLOBALS['user'];
  }

  switch ($op) {
    case 'create':
      return user_access('choose taxonomy term revisions', $account) && user_access("edit terms in {$entity->vid}", $account);

    case 'view':
      return user_access('view taxonomy term revisions', $account) && user_access('access content', $account);

    case 'use':
      return user_access('view taxonomy term revisions', $account) && user_access("edit terms in {$entity->vid}", $account);

    case 'update':
      return user_access('update taxonomy term revisions', $account) && user_access("edit terms in {$entity->vid}", $account);

    case 'delete':
      return user_access('delete taxonomy term revisions', $account) && user_access("edit terms in {$entity->vid}", $account);
  }

  return FALSE;
}

/**
 * Entity builder for the taxonomy term entity.
 *
 * @see taxonomy_revision_field_attach_form()
 * @see entity_revision_form_alter()
 */
function taxonomy_revision_build_term($entity_type, $entity, $form, &$form_state) {
  if ($entity_type != 'taxonomy_term') {
    return;
  }

  if (isset($form['revision_information']['revision']['#options'])) {
    $options = $form['revision_information']['revision']['#options'];
    if (
      count($options) == 1
      && $form_state['values']['revision'] == ENTITY_REVISION_UPDATE_AND_USE
      && _taxonomy_revision_enabled_by_default($entity->vid)
    ) {
      $form_state['values']['revision'] = ENTITY_REVISION_CREATE_AND_USE;
    }
  }
  if (!empty($form_state['values']['revision'])) {
    $entity->revision = $form_state['values']['revision'];
    if (!empty($form_state['values']['log'])) {
      $entity->log = check_plain($form_state['values']['log']);
    }
    else {
      $entity->log = '';
    }
  }
}

/**
 * Asks for confirmation to use revision.
 */
function taxonomy_revision_use_confirm($form, $form_state, $term_revision) {
  $form_state['term'] = $form['#term_revision'] = $term_revision;
  return confirm_form(
    $form,
    t('Are you sure you want to use the revision from %revision-date?', array('%revision-date' => format_date($term_revision->timestamp))),
    'taxonomy/term/' . $term_revision->tid . '/revisions',
    t('Current content will be able in Revisions list.'),
    t('Make actual'),
    t('Cancel')
  );
}


/**
 * Form submission handler for taxonomy_revision_use_confirm().
 *
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
function taxonomy_revision_use_confirm_submit($form, &$form_state) {
  $term = $form_state['term'];
  $term->revision = ENTITY_REVISION_UPDATE_AND_USE;

  taxonomy_term_save($term);

  watchdog('taxonomy_revision', 'used %title revision %revision.', array(
    '%title' => $term->name,
    '%revision' => $term->revision_id,
  ));
  drupal_set_message(t('Revision from %revision-date %title has been used.', array(
    '%revision-date' => format_date($term->timestamp),
    '%title' => $term->name,
  )));
  $form_state['redirect'] = 'taxonomy/term/' . $term->tid . '/revisions';
}

/**
 * Form submission handler for taxonomy_form_term().
 *
 * @see taxonomy_form_term()
 * @see taxonomy_form_term_validate()
 */
function taxonomy_revision_form_term_submit($form, &$form_state) {
  if ($form_state['triggering_element']['#value'] == t('Delete')) {
    $term = $form_state['term'];

    if ($term->current_revision) {
      // Execute the term deletion.
      if ($form_state['values']['delete'] === TRUE) {
        return taxonomy_term_confirm_delete_submit($form, $form_state);
      }
      // Rebuild the form to confirm term deletion.
      $form_state['rebuild'] = TRUE;
      $form_state['confirm_delete'] = TRUE;
      return;
    }
    else {
      drupal_goto("taxonomy/term/{$term->tid}/revisions/{$term->revision_id}/delete");
    }

    return;
  }

  $term = taxonomy_form_term_submit_build_taxonomy_term($form, $form_state);

  // Field collection fix to have actual entity in host entity.
  if (module_exists('field_collection')) {
    if (!isset($term->vocabulary_machine_name)) {
      $vocabulary = taxonomy_vocabulary_load($term->vid);
      $term->vocabulary_machine_name = $vocabulary->machine_name;
    }
    _entity_revision_update_field_collection_fields('taxonomy_term', $term);
  }

  if ($term->revision <= ENTITY_REVISION_CREATE_AND_USE) {
    $status = taxonomy_term_save($term);
    switch ($status) {
      case SAVED_NEW:
        drupal_set_message(t('Created new term %term.', array('%term' => $term->name)));
        watchdog('taxonomy', 'Created new term %term.', array('%term' => $term->name), WATCHDOG_NOTICE, l(t('edit'), 'taxonomy/term/' . $term->tid . '/edit'));
        break;

      case SAVED_UPDATED:
        drupal_set_message(t('Updated term %term.', array('%term' => $term->name)));
        watchdog('taxonomy', 'Updated term %term.', array('%term' => $term->name), WATCHDOG_NOTICE, l(t('edit'), 'taxonomy/term/' . $term->tid . '/edit'));
        // Clear the page and block caches to avoid stale data.
        cache_clear_all();
        break;
    }

    $current_parent_count = count($form_state['values']['parent']);
    $previous_parent_count = count($form['#term']['parent']);
    // Root doesn't count if it's the only parent.
    if ($current_parent_count == 1 && isset($form_state['values']['parent'][0])) {
      $current_parent_count = 0;
      $form_state['values']['parent'] = array();
    }

    // If the number of parents has been reduced to one or none, do a check on
    // the parents of every term in the vocabulary value.
    if ($current_parent_count < $previous_parent_count && $current_parent_count < 2) {
      taxonomy_check_vocabulary_hierarchy($form['#vocabulary'], $form_state['values']);
    }
    // If we've increased the number of parents and this is a single or flat
    // hierarchy, update the vocabulary immediately.
    elseif ($current_parent_count > $previous_parent_count && $form['#vocabulary']->hierarchy < 2) {
      $form['#vocabulary']->hierarchy = $current_parent_count == 1 ? 1 : 2;
      taxonomy_vocabulary_save($form['#vocabulary']);
    }
  }
  else {
    taxonomy_revision_term_save($term);
    $form_state['redirect'] = "taxonomy/term/{$term->tid}/revisions/{$term->revision_id}";
  }

  // Entity saved, all hooks are invoked - unset revision information.
  unset($term->revision);

  $form_state['values']['tid'] = $term->tid;
  $form_state['tid'] = $term->tid;
}

/**
 * Saves changes to a taxonomy term revision.
 *
 * @param object $term
 *   The taxonomy term object to be saved. If $term->revision_id is
 *   omitted (or $term->revision is ENTITY_REVISION_CREATE_NO_USE),
 *   a new taxonomy term revision will be added.
 */
function taxonomy_revision_term_save($term) {
  $transaction = db_transaction();

  try {
    // Revision can be created for existing content only.
    if (empty($term->tid)) {
      throw new Exception(t(
        'Revision can be created for existing content only! Use @function to create new content.',
        array('@function' => 'taxonomy_term_save()')
      ));
    }

    // Prevent leading and trailing spaces in term names.
    $term->name = trim($term->name);
    if (!isset($term->vocabulary_machine_name)) {
      $vocabulary = taxonomy_vocabulary_load($term->vid);
      $term->vocabulary_machine_name = $vocabulary->machine_name;
    }

    // Load the stored entity, if any.
    if (!empty($term->revision_id) && !isset($term->original)) {
      $result = entity_get_controller('taxonomy_term')->load(array($term->tid), array('revision_id' => $term->revision_id));
      $term->original = reset($result);
    }

    // Detect operation.
    if (empty($term->revision_id) || (isset($term->revision) && $term->revision == ENTITY_REVISION_CREATE_NO_USE)) {
      $op = 'insert';
      $term->revision = ENTITY_REVISION_CREATE_NO_USE;
      $term->current_revision = FALSE;
    }
    else {
      $op = 'update';
      $term->revision = ENTITY_REVISION_UPDATE_NO_USE;
    }

    field_attach_presave('taxonomy_term', $term);
    // Data is written in revision table in
    // entity_revision_taxonomy_term_presave().
    module_invoke_all('taxonomy_term_presave', $term);
    module_invoke_all('entity_presave', $term, 'taxonomy_term');

    // Set revision action for correct field processing.
    $term->entity_revision_op = $term->revision;
    $term->revision = (int) $term->revision == ENTITY_REVISION_CREATE_NO_USE;

    // Save fields.
    $function = "field_attach_{$op}";
    $function('taxonomy_term', $term);

    // Restore real revision action for entity hooks.
    $term->revision = $term->entity_revision_op;
    unset($term->entity_revision_op);

    // Invoke the taxonomy hooks.
    module_invoke_all("taxonomy_term_revision_$op", $term);
    module_invoke_all("entity_revision_$op", $term, 'taxonomy_term');
    unset($term->original);
  }
  catch (Exception $e) {
    $transaction->rollback();
    watchdog_exception('taxonomy_revision', $e);
    throw $e;
  }
}

/**
 * Generates an overview table of older revisions of a taxonomy term.
 *
 * @param object $term
 *   A taxonomy term object.
 *
 * @return array
 *   An array as expected by drupal_render().
 */
function taxonomy_revision_override_overview($term) {
  drupal_set_title(t('Revisions for %title', array('%title' => $term->name)), PASS_THROUGH);

  $header = array(
    'revision' => t('Revision'),
    'operations' => array('data' => t('Operations'), 'colspan' => 4),
  );

  $revisions = taxonomy_revision_list($term);
  $rows = array();

  $revert_permission = FALSE;
  if (user_access('revert taxonomy term revisions') || user_access('administer taxonomy')) {
    $revert_permission = TRUE;
  }

  foreach ($revisions as $revision) {
    $row = array();
    $operations = array();
    $term_revision = entity_revision_load('taxonomy_term', $revision->revision_id);

    if ($revision->revision_id == $term->revision_id) {
      $row[] = array(
        'data' => t('!date by !username', array(
            '!date' => l(format_date($revision->timestamp, 'short'), "taxonomy/term/$term->tid"),
            '!username' => theme('username', array('account' => user_load($revision->uid))),
          )) . (($revision->log != '') ? '<p class="revision-log">' . filter_xss($revision->log) . '</p>' : ''),
        'class' => array('revision-current'),
      );
      $operations[] = array(
        'data' => drupal_placeholder(t('current revision')),
        'class' => array('revision-current'),
        'colspan' => 4,
      );
    }
    else {
      $row[] = t('!date by !username', array(
          '!date' => l(format_date($revision->timestamp, 'short'), "taxonomy/term/$term->tid/revisions/$revision->revision_id/view"),
          '!username' => theme('username', array('account' => user_load($revision->uid))),
        )) . (($revision->log != '') ? '<p class="revision-log">' . filter_xss($revision->log) . '</p>' : '');
      if (entity_revision_access('update', 'taxonomy_term', $term_revision)) {
        $operations[] = l(t('edit'), "taxonomy/term/$term->tid/revisions/$revision->revision_id/edit");
      }
      if (entity_revision_access('use', 'taxonomy_term', $term_revision)) {
        $operations[] = l(t('make actual'), "taxonomy/term/$term->tid/revisions/$revision->revision_id/use");
      }
      if ($revert_permission) {
        $operations[] = l(t('revert'), "taxonomy/term/$term->tid/revisions/$revision->revision_id/revert");
      }
      if (entity_revision_access('delete', 'taxonomy_term', $term_revision)) {
        $operations[] = l(t('delete'), "taxonomy/term/$term->tid/revisions/$revision->revision_id/delete");
      }
    }
    $rows[] = array_merge($row, $operations);
  }
  $build['taxonomy_term_revisions_table'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => $header,
  );
  return $build;
}
