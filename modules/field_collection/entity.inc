<?php

/**
 * @file
 * Entity class revrite for field_collection_item.
 */

/**
 * Decorator class for EntityAPIController.
 */
class EntityRevisionEntityAPIControllerDecorator extends EntityAPIController {

  /**
   * Protected EntityAPIController::saveRevision() decorator.
   */
  public static function callSaveRevision(EntityAPIController $controller, $entity) {
    // Revision can be created for existing content only.
    if (empty($entity->item_id)) {
      throw new Exception(t(
        'Revision can be created for existing content only! Use @function to create new content.',
        array('@function' => 'EntityRevisionFieldCollectionItemEntity::save()')
      ));
    }

    // Load the stored entity, if any.
    if (!isset($entity->original) && !empty($entity->{$controller->revisionKey})) {
      $result = $controller->load(array($entity->{$controller->idKey}), array($controller->revisionKey => $entity->{$controller->revisionKey}));
      $entity->original = reset($result);
    }

    $entity->is_new_revision = empty($entity->{$controller->revisionKey})
      || (isset($entity->revision) && in_array((int) $entity->revision, array(
        ENTITY_REVISION_CREATE_AND_USE,
        ENTITY_REVISION_CREATE_NO_USE,
      ))
    );
    $controller->invoke('presave', $entity);

    $entity->is_new = FALSE;
    $entity->{$controller->defaultRevisionKey} = FALSE;
    unset($entity->revision);
    $return = $controller->saveRevision($entity);

    if ($entity->is_new_revision) {
      $hook = 'revision_insert';
      $entity->entity_revision_op = ENTITY_REVISION_CREATE_NO_USE;
      // Set revision action for correct field processing.
      $entity->revision = ENTITY_REVISION_CREATE_NO_USE;
      field_attach_insert($controller->entityType, $entity);
    }
    else {
      $hook = 'revision_update';
      $entity->entity_revision_op = ENTITY_REVISION_UPDATE_NO_USE;
      // Set revision action for correct field processing.
      $entity->revision = ENTITY_REVISION_UPDATE_AND_USE;
      field_attach_update($controller->entityType, $entity);
    }
    // Restore real revision action for entity hooks.
    $entity->revision = $entity->entity_revision_op;
    unset($entity->entity_revision_op);
    module_invoke_all($controller->entityType . '_' . $hook, $entity);
    module_invoke_all('entity_' . $hook, $entity, $controller->entityType);
    unset($entity->original);
    unset($entity->revision);

    return $return;
  }
}

/**
 * Class for field_collection_item entities.
 */
class EntityRevisionFieldCollectionItemEntity extends FieldCollectionItemEntity {

  /**
   * Overrides FieldCollectionItemEntity::save().
   */
  public function save($skip_host_save = FALSE) {
    // Make sure we have a host entity during creation.
    if (!empty($this->is_new) && !(isset($this->hostEntityId) || isset($this->hostEntity) || isset($this->hostEntityRevisionId))) {
      throw new Exception("Unable to create a field collection item without a given host entity.");
    }

    // Field collection fix to have actual entity in host entity.
    _entity_revision_update_field_collection_fields($this->entityType, $this);

    $host_entity = $this->hostEntity();

    $controller = entity_get_controller($this->entityType);
    // Mark item as archived to use revision key for host entity operations.
    $this->archived = TRUE;

    // Analyze revision action. Provide the same revision action as host entity.
    if (!empty($this->is_new) || !isset($host_entity->revision)) {
      $this->revision = ENTITY_REVISION_UPDATE_AND_USE;
    }
    else {
      $this->revision = isset($host_entity->entity_revision_op) ? $host_entity->entity_revision_op : $host_entity->revision;
    }

    // Save field collection item.
    switch ((int) $this->revision) {
      case ENTITY_REVISION_CREATE_NO_USE:
      case ENTITY_REVISION_UPDATE_NO_USE:
        // Call protected EntityAPIController::saveRevision() with decorator.
        $return = EntityRevisionEntityAPIControllerDecorator::callSaveRevision($controller, $this);
        break;

      case ENTITY_REVISION_CREATE_AND_USE:
      case ENTITY_REVISION_UPDATE_AND_USE:
        entity_revision_set_default('field_collection_item', $this);
        $return = $controller->save($this);
        break;

      default:
        throw new Exception("Unknown revision operation in entity revision property: {$this->revision}.");
    }

    // Entity saved, all hooks are invoked - clear revisioning information.
    unset($this->revision);

    // Update host entity, if needed.
    if (!$skip_host_save) {
      if (!$host_entity) {
        throw new Exception("Unable to save a field collection item without a valid reference to a host entity.");
      }
      // If this is saving revision only - do nothing, item exists and assigned
      // to the entity.
      if ($this->revision == ENTITY_REVISION_UPDATE_NO_USE) {
        return $return;
      }
      // Syncronize entity revision status.
      entity_revision_entity_load(array($this->hostEntityId => $host_entity), $this->hostEntityType);
      // Allow current entity update only.
      if (!$host_entity->current_revision || $this->revision == ENTITY_REVISION_CREATE_NO_USE) {
        throw new Exception("Unable update archive revision of field collection item host entity.");
      }
      // Prepare host entity for update.
      entity_revision_set_default($this->hostEntityType, $host_entity);
      $host_entity->revision = ENTITY_REVISION_UPDATE_AND_USE;
      // Assign data and save host entity.
      $delta = $this->delta();
      if (isset($delta)) {
        $host_entity->{$this->field_name}[$this->langcode][$delta] = array('entity' => $this);
      }
      else {
        $host_entity->{$this->field_name}[$this->langcode][] = array('entity' => $this);
      }
      entity_save($this->hostEntityType, $host_entity);
    }

    return $return;
  }
}
