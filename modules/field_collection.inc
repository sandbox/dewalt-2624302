<?php

/**
 * @file
 * Provides entity revision hooks for field_collection module support.
 */

/**
 * Implements hook_entity_revision_supported_entities().
 */
function field_collection_entity_revision_supported_entities() {
  return array('field_collection_item' => 'field_collection');
}

/**
 * Implements hook_entity_revision_entity_info_alter().
 *
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
function field_collection_entity_revision_entity_info_alter(&$info, $entity_type) {
  $info['entity class'] = 'EntityRevisionFieldCollectionItemEntity';
}
