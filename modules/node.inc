<?php

/**
 * @file
 * Provides entity revision hooks for node module support.
 */

/**
 * Implements hook_entity_revision_supported_entities().
 */
function node_entity_revision_supported_entities() {
  return array('node' => 'node');
}

/**
 * Implements hook_entity_revision_permission().
 */
function node_entity_revision_permission() {
  return array(
    'create revisions' => array(
      'title' => t('Create content revisions'),
    ),
    'update revisions' => array(
      'title' => t('Update content revisions'),
    ),
    'update own revisions' => array(
      'title' => t('Update own content revisions'),
    ),
    'delete own revisions' => array(
      'title' => t('Delete own content revisions'),
    ),
  );
}

/**
 * Implements hook_entity_revision_menu_alter().
 */
function node_entity_revision_menu_alter(&$items, $entity_type) {
  if ($entity_type != 'node') {
    return;
  }

  $items['node/%node/revisions'] = array(
    'page callback' => 'node_revision_override_overview',
    'access callback' => 'entity_revision_access',
    'access arguments' => array('view', 'node', 1),
  ) + $items['node/%node/revisions'];

  $items['node/%node/revisions/%'] = array(
    'title callback' => 'entity_revision_title',
    'title arguments' => array('node', 1),
    'access callback' => 'entity_revision_access',
    'access arguments' => array('view', 'node', 1),
  ) + $items['node/%node/revisions/%/view'];

  $items['node/%node/revisions/%/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['node/%node/revisions/%/edit'] = array(
    'title' => 'Edit',
    'load arguments' => array(3),
    'page callback' => 'node_page_edit',
    'page arguments' => array(1),
    'access callback' => 'entity_revision_access',
    'access arguments' => array('update', 'node', 1),
    'weight' => 0,
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    'file' => 'node.pages.inc',
    'file path' => drupal_get_path('module', 'node'),
  );

  $items['node/%node/revisions/%/use'] = array(
    'title' => 'Make actual',
    'load arguments' => array(3),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('node_revision_use_confirm', 1),
    'access callback' => 'entity_revision_access',
    'access arguments' => array('use', 'node', 1),
    'weight' => 1,
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
  );

  $items['node/%node/revisions/%/delete'] = array(
    'access callback' => 'entity_revision_access',
    'access arguments' => array('delete', 'node', 1),
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
  ) + $items['node/%node/revisions/%/delete'];
}

/**
 * Implements hook_entity_revision_form_callbacks_replacements().
 */
function node_entity_revision_form_callbacks_replacements() {
  return array(
    'node_form' => array(
      'entity_type' => 'node',
      'entity_key' => 'node',
      'submit' => array(
        'node_form_submit' => 'node_revision_form_submit',
        'node_form_delete_submit' => 'node_revision_form_delete_submit',
      ),
    ),
  );
}

/**
 * Implements hook_entity_revision_options_alter().
 */
function node_entity_revision_options_alter(&$options, $entity_type, $entity) {
  if ($entity_type != 'node') {
    return;
  }
  // Provide administator-configured behaviour for non-privileged editor.
  if (!empty($entity->revision) && count($options) == 1 && isset($options[ENTITY_REVISION_UPDATE_AND_USE])) {
    $options = array(ENTITY_REVISION_CREATE_AND_USE => t('Create new revision and use it'));
  }
}

/**
 * Implements hook_entity_revision_access().
 *
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
function node_entity_revision_access($op, $entity_type, $entity, $account) {
  if (user_access('administer nodes', $account)) {
    return TRUE;
  }

  // If no user object is supplied, the access check is for the current user.
  if (empty($account)) {
    $account = $GLOBALS['user'];
  }

  $current_node = node_load($entity->nid);
  $owner = $entity->uid == $account->uid;
  switch ($op) {
    case 'create':
      return user_access('create revisions', $account) && node_access('view', $current_node, $account);

    case 'view':
      return user_access('view revisions', $account) && node_access('view', $current_node, $account);

    case 'use':
      return user_access('view revisions', $account) && node_access('update', $current_node, $account);

    case 'update':
    case 'delete':
      return (
        user_access("$op revisions", $account)
        || ($owner && user_access("$op own revisions", $account))
      ) && node_access('view', $current_node, $account);
  }

  return FALSE;
}

/**
 * Asks for confirmation to use revision.
 *
 * @param object $node
 *   The node revision.
 *
 * @return array
 *   An array as expected by drupal_render().
 *
 * @ingroup forms
 */
function node_revision_use_confirm($form, &$form_state, $node) {
  $form_state['node'] = $form['#node'] = $node;
  return confirm_form(
    $form,
    t('Are you sure you want to use revision from %revision-date?', array('%revision-date' => format_date($node->revision_timestamp))),
    'node/' . $node->nid . '/revisions',
    t('Current content will be able in Revisions list.'),
    t('Make actual'),
    t('Cancel')
  );
}

/**
 * Form submission handler for node_revision_use_confirm().
 *
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
function node_revision_use_confirm_submit($form, &$form_state) {
  $node = $form_state['node'];
  $node->revision = ENTITY_REVISION_UPDATE_AND_USE;

  node_save($node);

  watchdog('content', '@type: used %title revision %revision.', array(
    '@type' => $node->type,
    '%title' => $node->title,
    '%revision' => $node->vid,
  ));
  drupal_set_message(t(
    '@type %title has been used revision from %revision-date.',
    array(
      '@type' => node_type_get_name($node),
      '%title' => $node->title,
      '%revision-date' => format_date($node->revision_timestamp),
    )
  ));
  $form_state['redirect'] = 'node/' . $node->nid . '/revisions';
}

/**
 * Form submission handler for node_form().
 *
 * @see node_form()
 * @see node_form_validate()
 */
function node_revision_form_submit($form, &$form_state) {
  $node = node_form_submit_build_node($form, $form_state);
  $insert = empty($node->nid);

  // Field collection fix to have actual entity in host entity.
  if (module_exists('field_collection')) {
    _entity_revision_update_field_collection_fields('node', $node);
  }

  if ($node->revision <= ENTITY_REVISION_CREATE_AND_USE) {
    node_save($node);
    $node_link = l(t('view'), 'node/' . $node->nid);
    $revision_only = FALSE;
  }
  else {
    node_revision_save($node);
    $node_link = l(t('view'), 'node/' . $node->nid . '/revisions/' . $node->vid);
    $revision_only = TRUE;
  }

  $watchdog_args = array('@type' => $node->type, '%title' => t('revision') . ' ' . $node->title);
  $t_args = array('@type' => node_type_get_name($node), '%title' => $node->title);

  if ($insert) {
    watchdog('content', '@type: added %title.', $watchdog_args, WATCHDOG_NOTICE, $node_link);
    drupal_set_message(t('@type %title has been created.', $t_args));
  }
  elseif ($revision_only) {
    watchdog('content', '@type: revision updated %title.', $watchdog_args, WATCHDOG_NOTICE, $node_link);
    drupal_set_message(t('@type revision %title has been updated.', $t_args));
  }
  else {
    watchdog('content', '@type: updated %title.', $watchdog_args, WATCHDOG_NOTICE, $node_link);
    drupal_set_message(t('@type %title has been updated.', $t_args));
  }
  if ($node->nid) {
    $form_state['values']['nid'] = $node->nid;
    $form_state['nid'] = $node->nid;
    $form_state['values']['vid'] = $node->vid;
    $form_state['vid'] = $node->vid;
    if ($revision_only) {
      $form_state['redirect'] = entity_revision_access('view', 'node', $node) ?
        'node/' . $node->nid . '/revisions/' . $node->vid :
        '<front>';
    }
    else {
      $form_state['redirect'] = node_access('view', $node) ? 'node/' . $node->nid : '<front>';
    }
  }
  else {
    // In the unlikely case something went wrong on save, the node will be
    // rebuilt and node form redisplayed the same way as in preview.
    drupal_set_message(t('The post could not be saved.'), 'error');
    $form_state['rebuild'] = TRUE;
  }
  // Clear the page and block caches.
  cache_clear_all();
}

/**
 * Form submission handler for node_form().
 *
 * Handles the 'Delete' button on the node form.
 *
 * @see node_form()
 * @see node_form_validate()
 *
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
function node_revision_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $node = $form_state['node'];
  if ($node->current_revision) {
    $form_state['redirect'] = array(
      'node/' . $node->nid . '/delete',
      array('query' => $destination),
    );
  }
  else {
    $form_state['redirect'] = array(
      'node/' . $node->nid . '/revisions/' . $node->vid . '/delete',
      array('query' => $destination),
    );
  }
}

/**
 * Saves changes to a node revision.
 *
 * @param object $node
 *   The $node object to be saved. If $node->vid is
 *   omitted (or $node->revision is ENTITY_REVISION_CREATE_NO_USE),
 *   a new node revision will be added.
 */
function node_revision_save($node) {
  $transaction = db_transaction();

  try {
    // Revision can be created for existing content only.
    if (empty($node->nid)) {
      throw new Exception(t(
        'Revision can be created for existing content only! Use @function to create new content.',
        array('@function' => 'node_save()')
      ));
    }

    // Load the stored entity, if any.
    if (!isset($node->original) && !empty($node->vid)) {
      $result = entity_get_controller('node')->load(array($node->nid), array('vid' => $node->vid));
      $node->original = reset($result);
    }

    field_attach_presave('node', $node);
    global $user;

    // Determine if we will be inserting a new node revision.
    $node->is_new_revision = empty($node->vid) || (isset($node->revision) && $node->revision == ENTITY_REVISION_CREATE_NO_USE);

    // Set the timestamp fields.
    if ($node->is_new_revision || empty($node->created)) {
      $node->created = REQUEST_TIME;
    }
    // The changed timestamp is always updated for bookkeeping purposes,
    // for example: revisions, searching, etc.
    $node->changed = REQUEST_TIME;
    $node->timestamp = REQUEST_TIME;

    // Let modules modify the node before it is saved to the database.
    module_invoke_all('node_presave', $node);
    module_invoke_all('entity_presave', $node, 'node');

    // The property can be modified in presave hooks.
    if (empty($node->vid)) {
      $node->is_new_revision = TRUE;
    }
    if ($node->is_new_revision) {
      if (!isset($node->log)) {
        $node->log = '';
      }
    }
    elseif (!isset($node->log) || $node->log === '') {
      unset($node->log);
    }

    // When saving a new node revision, unset any existing $node->vid so as to
    // ensure that a new revision will actually be created, then store the old
    // revision ID in a separate property for use by node hook implementations.
    if ($node->is_new_revision && $node->vid) {
      $node->old_vid = $node->vid;
      unset($node->vid);
    }

    // Save the node and node revision.
    if ($node->is_new_revision) {
      _node_save_revision($node, $user->uid);
      $node->entity_revision_op = ENTITY_REVISION_CREATE_NO_USE;
      // Set revision action for correct field processing.
      $node->revision = ENTITY_REVISION_CREATE_NO_USE;
      $node->current_revision = FALSE;
      $op = 'revision_insert';
      $field_op = 'insert';
    }
    else {
      _node_save_revision($node, $user->uid, 'vid');
      $node->entity_revision_op = ENTITY_REVISION_UPDATE_NO_USE;
      // Set revision action for correct field processing.
      $node->revision = ENTITY_REVISION_UPDATE_AND_USE;
      $op = 'revision_update';
      $field_op = 'update';
    }

    // Call the node specific callback (if any). This can be
    // node_invoke($node, 'insert') or
    // node_invoke($node, 'update').
    node_invoke($node, $op);

    // Save fields.
    $function = "field_attach_{$field_op}";
    $function('node', $node);

    // Restore real revision action for entity hooks.
    $node->revision = $node->entity_revision_op;
    unset($node->entity_revision_op);
    module_invoke_all('node_' . $op, $node);
    module_invoke_all('entity_' . $op, $node, 'node');

    // Clear internal properties.
    unset($node->is_new_revision);
    unset($node->original);

    // Ignore slave server temporarily to give time for the
    // saved node to be propagated to the slave.
    db_ignore_slave();
  }
  catch (Exception $e) {
    $transaction->rollback();
    watchdog_exception('node', $e);
    throw $e;
  }
}

/**
 * Generates an overview table of revisions of a node.
 *
 * @param object $node
 *   A node object.
 *
 * @return array
 *   An array as expected by drupal_render().
 *
 * @see node_menu()
 */
function node_revision_override_overview($node) {
  drupal_set_title(t('Revisions for %title', array('%title' => $node->title)), PASS_THROUGH);

  $header = array(
    t('Revision'),
    array(
      'data' => t('Operations'),
      'colspan' => 4,
    ),
  );

  $revisions = node_revision_list($node);
  $rows = array();

  $revert_permission = FALSE;
  if ((user_access('revert revisions') || user_access('administer nodes')) && node_access('update', $node)) {
    $revert_permission = TRUE;
  }

  foreach ($revisions as $revision) {
    $row = array();
    $operations = array();
    $node_revision = entity_revision_load('node', $revision->vid);

    if ($revision->current_vid > 0) {
      $row[] = array(
        'data' => t('!date by !username', array(
          '!date' => l(format_date($revision->timestamp, 'short'), "node/$node->nid"),
          '!username' => theme('username', array('account' => $revision)),
        )) . (($revision->log != '') ? '<p class="revision-log">' . filter_xss($revision->log) . '</p>' : ''),
        'class' => array('revision-current'),
      );
      $operations[] = array(
        'data' => drupal_placeholder(t('current revision')),
        'class' => array('revision-current'),
        'colspan' => 4,
      );
    }
    else {
      $row[] = t('!date by !username', array(
        '!date' => l(format_date($revision->timestamp, 'short'),
          "node/$node->nid/revisions/$revision->vid/view"),
        '!username' => theme('username', array('account' => $revision)),
      )) . (($revision->log != '') ? '<p class="revision-log">' . filter_xss($revision->log) . '</p>' : '');
      if (entity_revision_access('update', 'node', $node_revision)) {
        $operations[] = l(t('edit'), "node/$node->nid/revisions/$revision->vid/edit");
      }
      if (entity_revision_access('use', 'node', $node_revision)) {
        $operations[] = l(t('make actual'), "node/$node->nid/revisions/$revision->vid/use");
      }
      if ($revert_permission) {
        $operations[] = l(t('revert'), "node/$node->nid/revisions/$revision->vid/revert");
      }
      if (entity_revision_access('delete', 'node', $node_revision)) {
        $operations[] = l(t('delete'), "node/$node->nid/revisions/$revision->vid/delete");
      }
    }
    $rows[] = array_merge($row, $operations);
  }

  $build['node_revisions_table'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => $header,
  );

  return $build;
}
