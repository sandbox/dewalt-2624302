<?php

/**
 * @file
 * Hooks provided by Enity Revision module.
 */

/**
 * Provide entity type supporting module.
 *
 * The hooks defines, that entity can be processed with entity_revision module,
 * and defines the module, which provides entity support throught
 * entity_revision API. The module, defined in this hook, will be used to
 * search for other entity_revision hooks implementations.
 *
 * Also the hook can be used to cancel entity type processing with
 * entity_revision module by providing empty value instead of module name.
 * (Make sure, that implementation invokes later, than one, where module
 * name is defined).
 *
 * @return array
 *   An assosiation of entity type and module, supporting it. Note: only one
 *   module (from last invoked hook implementation) will be used for entity
 *   type support.
 *
 * @see entity_revision_get_supported_entities()
 */
function hook_entity_revision_supported_entities() {
  return array('node' => 'my_module');
}

/**
 * Alter the entity info by active supportin module only.
 *
 * Invokes for supporting module only!
 * (Supporting module defines in hook_entity_revision_supported_entities()).
 *
 * Supporting module may implement this hook to alter the information that
 * defines an entity. All properties that are available in hook_entity_info()
 * can be altered here.
 *
 * @param array $info
 *   The entity type info array.
 * @param string $entity_type
 *   The type of entity.
 *
 * @see entity_revision_entity_info_alter()
 */
function hook_entity_revision_entity_info_alter(&$info, $entity_type) {
  if ($entity_type == 'field_collection_item') {
    $info['entity class'] = 'EntityRevisionFieldCollectionItemEntity';
  }
}

/**
 * Allows supporting module to define user permissions.
 *
 * Permissions, defined with the hook, will be abled in 'Entity Revision'
 * section to configure.
 *
 * Invokes for supporting module only!
 * (Supporting module defines in hook_entity_revision_supported_entities()).
 *
 * @param string $entity_type
 *   The type of entity.
 *
 * @return array
 *   An array in format hook_permission() requires.
 *
 * @see entity_revision_permission()
 * @see hook_permission()
 */
function hook_entity_revision_permission($entity_type) {
  if ($entity_type == 'node') {
    return array(
      'create revisions' => array(
        'title' => t('Create content revisions'),
      ),
      'update revisions' => array(
        'title' => t('Update content revisions'),
      ),
    );
  }
}

/**
 * Alter the data being saved to the {menu_router} table.
 *
 * Invokes for supporting module only!
 * (Supporting module defines in hook_entity_revision_supported_entities()).
 *
 * Allows create pages to support operations with entity revisions. Invoking
 * hook only for supporting modules allow to have different entity type support
 * implementations without conflict between them.
 *
 * @param array $items
 *   Associative array of menu router definitions returned from hook_menu().
 * @param string $entity_type
 *   The type of entity.
 *
 * @see entity_revision_menu_alter()
 */
function hook_entity_revision_menu_alter(&$items, $entity_type) {
  if ($entity_type == 'node') {
    $items['node/%node/revisions/%/edit'] = array(
      'title' => 'Edit',
      'load arguments' => array(3),
      'page callback' => 'node_page_edit',
      'page arguments' => array(1),
      'access callback' => 'entity_revision_access',
      'access arguments' => array('update', 'node', 1),
      'file' => 'node.pages.inc',
      'file path' => drupal_get_path('module', 'node'),
    );
  }
}

/**
 * Define entity type form IDs and custom callbacks for the forms.
 *
 * Invokes for supporting module only!
 * (Supporting module defines in hook_entity_revision_supported_entities()).
 *
 * Define form bases IDs for supported entity type, that should be modified by
 * entity_revision module to work with revisions. Stores a list of form
 * callbacks, that should be replaced for correct data processing, and
 * callbacks assosiation with replacements. Replacement process affects each
 * element in the form (including the form), having '#validate' or '#submit'
 * properties.
 * The data structure, the hook defines, is array with the next keys:
 *   - "entity_type" (required): Defines entity type, the form modifies.
 *   - "entity_key" (optional): Provides entity revision object situation in
 *   $form_state container. Use "][" to separate parents.
 *   - "entity_callback" (optional): Custom function, that extracts entity from
 *   form structure or $form_state container. Form structure is passed to
 *   function as first argument, $form_state - as second.
 *   - "validate" (optional): A list of validation callbacks to replace with
 *   search string in key and replacement string in value. As a rule,
 *   replacement callbacks should be defined in module, implementing the hook.
 *   - "submit" (optional): A list of submit callbacks to replace with
 *   search string in key and replacement string in value. As a rule,
 *   replacement callbacks should be defined in module, implementing the hook.
 *   - "entity_builder" (optional): A list of entity builders to replace
 *   with search string in key and replacement string in value. As a rule,
 *   replacement callbacks should be defined in module, implementing the hook.
 *
 * @return array
 *   Data array, wiht described above structure.
 *
 * @see entity_revision_form_alter()
 */
function hook_entity_revision_form_callbacks_replacements() {
  return array(
    'node_form' => array(
      'entity_type' => 'node',
      'entity_key' => 'node',
      'submit' => array(
        'node_form_submit' => 'node_revision_form_submit',
        'node_form_delete_submit' => 'node_revision_form_delete_submit',
      ),
    ),
  );
}

/**
 * Determines whether the user may perform the operation on the revision entity.
 *
 * Invokes for supporting module only!
 * (Supporting module defines in hook_entity_revision_supported_entities()).
 *
 *
 * @param string $op
 *   The operation to be performed on the entity. Possible values are:
 *   - "view"
 *   - "use"
 *   - "update"
 *   - "delete"
 *   - "create"
 *   Note: operations "update", "delete", "use" aren't applied to actual
 *   entity.
 * @param string $entity_type
 *   The type of entity on which the operation is to be performed.
 * @param object $entity
 *   The entity revision on which the operation is to be performed.
 * @param object $account
 *   A user object representing the user for whom the operation is to
 *   be performed.
 *
 * @return bool
 *   TRUE if the operation may be performed, FALSE otherwise.
 *
 * @see entity_revision_access()
 */
function hook_entity_revision_access($op, $entity_type, $entity, $account) {
  if ($entity_type == 'node') {
    return user_access("$op revisions", $account) && node_access('update', $entity, $account);
  }
  return FALSE;
}

/**
 * Allows other modules modify access to perform entity operations.
 *
 * Invokes after access check to operation with actual entity and
 * to operation with revision of the entity. Doesn't affects administration
 * access to entity.
 *
 * @param bool $access
 *   Current access state to modify it.
 * @param string $op
 *   The operation to be performed on the entity. Possible values are:
 *   - "view"
 *   - "update"
 *   - "delete"
 *   - "create"
 * @param string $entity_type
 *   The type of entity on which the operation is to be performed.
 * @param object $entity
 *   The entity revision on which the operation is to be performed.
 * @param object $account
 *   A user object representing the user for whom the operation is to
 *   be performed.
 *
 * @see hook_entity_revision_access()
 * @see entity_revision_access()
 */
function hook_entity_revision_access_alter(&$access, $op, $entity_type, $entity, $account) {
  if ($access && $entity_type == 'node' && $entity->status != NODE_PUBLISHED) {
    $access = FALSE;
  }
}

/**
 * Allow other modules to modify revisioning options list.
 *
 * Basicly, full options list is the next:
 * @code
 * array(
 *   ENTITY_REVISION_UPDATE_AND_USE => t('Save revision and use it'),
 *   ENTITY_REVISION_CREATE_AND_USE => t('Create new revision and use it'),
 *   ENTITY_REVISION_UPDATE_NO_USE => t('Save revision'),
 *   ENTITY_REVISION_CREATE_NO_USE => t('Create new revision'),
 * );
 * @endcode
 * but it is limited with user access before be passed to the function.
 *
 * @param array $options
 *   Revisioning options for entity revision, allowed for user.
 * @param string $entity_type
 *   The type of entity.
 * @param object $entity
 *   The entity revision object.
 *
 * @see entity_revision_form_alter()
 */
function hook_entity_revision_options_alter(&$options, $entity_type, $entity) {
  if (isset($options[ENTITY_REVISION_UPDATE_NO_USE])) {
    if ($entity_type == 'node' && !$entity->current_revision) {
      $options[ENTITY_REVISION_UPDATE_NO_USE] = t('Do nothing');
    }
  }
}
