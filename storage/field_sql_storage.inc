<?php

/**
 * @file
 * Entity Revision SQL implementation of the field storage API.
 */

/**
 * Implements hook_field_storage_info_alter().
 */
function entity_revision_field_storage_info_alter(&$info) {
  $info['field_sql_storage']['module'] = 'entity_revision';
}

/**
 * Implements hook_field_storage_create_field().
 */
function entity_revision_field_storage_create_field($field) {
  return field_sql_storage_field_storage_create_field($field);
}

/**
 * Implements hook_field_update_forbid().
 *
 * Forbid any field update that changes column definitions if there is
 * any data.
 */
function entity_revision_field_update_forbid($field, $prior_field, $has_data) {
  return field_sql_storage_field_update_forbid($field, $prior_field, $has_data);
}

/**
 * Implements hook_field_storage_update_field().
 */
function entity_revision_field_storage_update_field($field, $prior_field, $has_data) {
  return field_sql_storage_field_storage_update_field($field, $prior_field, $has_data);
}

/**
 * Implements hook_field_storage_delete_field().
 */
function entity_revision_field_storage_delete_field($field) {
  return field_sql_storage_field_storage_delete_field($field);
}

/**
 * Implements hook_field_storage_load().
 */
function entity_revision_field_storage_load($entity_type, $entities, $age, $fields, $options) {
  return field_sql_storage_field_storage_load($entity_type, $entities, $age, $fields, $options);
}

/**
 * Implements hook_field_storage_delete().
 *
 * This function deletes data for all fields for an entity from the database.
 */
function entity_revision_field_storage_delete($entity_type, $entity, $fields) {
  return field_sql_storage_field_storage_delete($entity_type, $entity, $fields);
}

/**
 * Implements hook_field_storage_purge().
 *
 * This function deletes data from the database for a single field on
 * an entity.
 */
function entity_revision_field_storage_purge($entity_type, $entity, $field, $instance) {
  return field_sql_storage_field_storage_purge($entity_type, $entity, $field, $instance);
}

/**
 * Implements hook_field_storage_query().
 */
function entity_revision_field_storage_query(EntityFieldQuery $query) {
  field_sql_storage_field_storage_query($query);
}

/**
 * Implements hook_field_storage_delete_revision().
 *
 * This function actually deletes the data from the database.
 */
function entity_revision_field_storage_delete_revision($entity_type, $entity, $fields) {
  return field_sql_storage_field_storage_delete_revision($entity_type, $entity, $fields);
}

/**
 * Implements hook_field_storage_delete_instance().
 *
 * This function simply marks for deletion all data associated with the field.
 */
function entity_revision_field_storage_delete_instance($instance) {
  return field_sql_storage_field_storage_delete_instance($instance);
}

/**
 * Implements hook_field_attach_rename_bundle().
 */
function entity_revision_field_attach_rename_bundle($entity_type, $bundle_old, $bundle_new) {
  return field_sql_storage_field_attach_rename_bundle($entity_type, $bundle_old, $bundle_new);
}

/**
 * Implements hook_field_storage_purge_field().
 *
 * All field data items and instances have already been purged, so all
 * that is left is to delete the table.
 */
function entity_revision_field_storage_purge_field($field) {
  return field_sql_storage_field_storage_purge_field($field);
}

/**
 * Implements hook_field_storage_details().
 */
function entity_revision_field_storage_details($field) {
  return field_sql_storage_field_storage_details($field);
}

/**
 * Implements hook_field_storage_write().
 */
function entity_revision_field_storage_write($entity_type, $entity, $op, $fields) {
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
  if (!isset($vid)) {
    $vid = $id;
  }
  // Data in $entity->current_revision can be expired on this step
  // - syncronize it.
  entity_revision_entity_load(array($id => $entity), $entity_type);

  foreach ($fields as $field_id) {
    $field = field_info_field_by_id($field_id);
    $field_name = $field['field_name'];
    $table_name = _field_sql_storage_tablename($field);
    $revision_name = _field_sql_storage_revision_tablename($field);

    $all_languages = field_available_languages($entity_type, $field);
    $field_languages = array_intersect($all_languages, array_keys((array) $entity->$field_name));

    // Delete and insert, rather than update, in case a value was added.
    if ($op == FIELD_STORAGE_UPDATE) {
      // Delete languages present in the incoming $entity->$field_name.
      // Delete all languages if $entity->$field_name is empty.
      $languages = !empty($entity->$field_name) ? $field_languages : $all_languages;
      if ($languages) {
        if ($entity->current_revision) {
          db_delete($table_name)
            ->condition('entity_type', $entity_type)
            ->condition('entity_id', $id)
            ->condition('language', $languages, 'IN')
            ->execute();
        }
        db_delete($revision_name)
          ->condition('entity_type', $entity_type)
          ->condition('entity_id', $id)
          ->condition('revision_id', $vid)
          ->condition('language', $languages, 'IN')
          ->execute();
      }
    }

    // Prepare the multi-insert query.
    $do_insert = FALSE;
    $columns = array(
      'entity_type',
      'entity_id',
      'revision_id',
      'bundle',
      'delta',
      'language',
    );
    foreach ($field['columns'] as $column => $attributes) {
      $columns[] = _field_sql_storage_columnname($field_name, $column);
    }
    $query = db_insert($table_name)->fields($columns);
    $revision_query = db_insert($revision_name)->fields($columns);

    foreach ($field_languages as $langcode) {
      $items = (array) $entity->{$field_name}[$langcode];
      $delta_count = 0;
      foreach ($items as $delta => $item) {
        // We now know we have someting to insert.
        $do_insert = TRUE;
        $record = array(
          'entity_type' => $entity_type,
          'entity_id' => $id,
          'revision_id' => $vid,
          'bundle' => $bundle,
          'delta' => $delta,
          'language' => $langcode,
        );
        foreach ($field['columns'] as $column => $attributes) {
          $record[_field_sql_storage_columnname($field_name, $column)] = isset($item[$column]) ? $item[$column] : NULL;
        }
        $query->values($record);
        if (isset($vid)) {
          $revision_query->values($record);
        }

        if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED && ++$delta_count == $field['cardinality']) {
          break;
        }
      }
    }

    // Execute the query if we have values to insert.
    if ($do_insert) {
      if ($entity->current_revision) {
        $query->execute();
      }
      $revision_query->execute();
    }
  }
}
